module redis-load-test

go 1.16

require (
	github.com/go-redis/redis/v8 v8.11.4
	go.k6.io/k6 v0.34.1
)
