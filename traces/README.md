* sharedstate-trace1.json: an older trace from the persistent/shared Redis.  Probably not useful now
* sharedstate-trace2.json: the trace used by https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/1346 to generate the bulk of the results
* sharedstate-no-session-trace.json: sharedstate-trace2.json without sessions
* sharedstate-no-session-no-ci-pubsub.json: sharedstate-trace2.json without sessions and no publishing to `workhorse:notifications`
* cache-trace.json: A simple trace from the Cache Redis
* ratelimiting-trace.json: A simple trace from the Ratelimiting Redis
