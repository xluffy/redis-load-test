["setex", "sadd", "get", "set", "pfadd", "expire", "hget", "exists", "incrbyfloat", "incr", "publish", "del", "zadd", "zcard", "incrby", "zremrangebyrank", "decr", "smembers", "mget", "srem", "hset", "subscribe", "unsubscribe", "zrevrange", "scard", "pexpire", "hdel", "blpop", "zrangebyscore", "zremrangebyscore"]

def command_data(data, cmd, min: nil, max: nil)
  distribution = {}
  data[cmd].values.each do |dis|
    dis.each do |key, value|
      distribution[key.to_i] ||= 0
      distribution[key.to_i] += value
    end
  end
  if distribution.keys.length > 15
    min ||= distribution.keys.min
    max ||= distribution.keys.max
    (min..max).each_slice((max - min) / 15) do |slice|
      sum = 0
      distribution.each do |key, value|
        sum += value if slice.min <= key && key <= slice.max
      end
      puts "#{slice.min}-#{slice.max},#{sum}"
    end
  else
    distribution.to_a.sort_by(&:first).each do |key, value|
      puts "#{key},#{value}"
    end
  end
end

# command_data(data, 'setex', min: 1, max: 1283)
# command_data(data, 'sadd')
command_data(data, 'hset')
