FROM golang:1.17

WORKDIR /go/src/app
COPY . .

RUN go install
RUN go install go.k6.io/xk6/cmd/xk6@latest
RUN xk6 build --output ./_build/k6 --with github.com/k6io/xk6-redis=.

ENTRYPOINT ["./_build/k6", "run", "./script.js", "--quiet", "--env", "DURATION=120s", "--env", "MULTIPLIER=1", "--env", "WORKHORSE_SUBSCRIBERS=100", "--env", "TRACE_DURATION=21s"]
CMD ["--env", "TRACE_PATH=./traces/cache-trace-2021-11-05.json"]
