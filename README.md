# Redis Load Test

![Intro](./images/intro.png)

For https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/1346

This repository stores a set of Redis load tests. The load tests simulate the
traffic of GitLab's Redis instances on production.

It uses [k6](https://k6.io/) but via [xk6](https://github.com/grafana/xk6) to build a custom version which includes Redis connection capability.

## Install

* Ensure go is installed (1.16 or later)
* Build/install dependencies and xk6.  From a clone of this repo:

```bash
go install
go install go.k6.io/xk6/cmd/xk6@latest
```

* NB: You may need to update your PATH to add the golang packages bin directory
  e.g. if you're using ASDF this might be `~/.asdf/install/golang/<version>/packages/bin`
* Build k6 with redis extension. If `./redis.go` is touch, rebuild the binary.

```bash
xk6 build --output ./_build/k6 --with github.com/k6io/xk6-redis=.
```

## Run a load test

Run load test script with the built binary

```bash
./_build/k6 run ./script.js --quiet
```

* By default, the load test runs for 1 minute. It supports some options:

```bash
./_build/k6 run ./script.js \
        --quiet \
        --env DURATION=30s \
        --env REDIS_URL=host:port # Or REDIS_CLUSTER_URL; see below\
        --env REDIS_PASSWORD=password \
        --env REDIS_DB=0 \
        --env TRACE_PATH=./traces/sharedstate-trace1.json \
        --env TRACE_DURATION=30s \
        --env MULTIPLIER=1 \
        --env WORKHORSE_SUBSCRIBERS=300 \
        --summary-trend-stats="avg,max,med,p(95),p(99),p(99.99)" \
```

* `DURATION`: How long the stress test should run.
* `REDIS_URL`: The non-Cluster Redis to use (if not localhost:6379)
* `REDIS_CLUSTER_NODE`: Any node from a Redis Cluster to boot-strap connectivity; overrides REDIS_URL and turns on Cluster mode.
* `TRACE_PATH`: The path to the trace file.
* `TRACE_DURATION`: The load test calculates the request rate based on the
  captured trace file. It assumes that the trace file is captured from tcpdump
  command for 30 seconds. If that's not the case, adjust this parameter to
  match the recording time.  Or use this to adjust the actual generated traffic
  to match your test scenario; raising this number reduces the rate of commands
  sent to Redis, and dropping it increases the rate.
* `MULTIPLIER`: increases the load we push to redis in integer units.  Similar
  in result to dropping TRACE_DURATION but less fine-grained.
* `WORKHORSE_SUBSCRIBERS`: how many active subscribers should listen to workhorse:notifications long-term (duration of the test)
   It is up to you to balance this across however many load generators you are running to get the total you desire.  For gitlab.com, see [count(kube_pod_container_state_started{env="gprd", container="gitlab-workhorse"})](https://thanos.gitlab.net/graph?g0.expr=count(kube_pod_container_state_started%7Benv%3D%22gprd%22%2C%20container%3D%22gitlab-workhorse%22%7D)&g0.tab=0&g0.stacked=0&g0.range_input=1d&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D) to see what normal is (at this writing, peaks in the range of 550-600 workers are normal)

Note that the workhorse subscriber feature means we've got a lot of go-routines in a long-run scenario; you may need to Ctrl-C twice to quit once the main load test is running (after the `Preparing data` steps are done)

### Reading the result

![Result](./images/result.png)

* `iterations`: how many requests we triggered
* `dropped_iterations`: how many times requests couldn't be sent fast enough.
   At the scale we're running this, there will almost always be some of these,
   but if it gets 'too high' a proportion of `iterations` if Redis still has
   capacity, consider adding  more load generators in parallel, and reducing
   how much load/traffic each is generating.  
* `iteration_duration`: the duration of each request
* `redis_counter_x`: the counter counting x-command requests
* `redis_latency_x`: the latency histogram of x-command requests. The report can be tweaked with `summary-trend-stats` option

### Metrics

Without limiting possibilties, the following is what we did in the original tests:

1. On the target Redis, run 
   1. [redis_exporter](https://github.com/oliver006/redis_exporter)
   1. [process-exporter](https://github.com/ncabatoff/process-exporter)
     * Run with `-threads=true` and `-gather-smaps=false`, and a config file supplied with --config.path with contents:
```yaml
---
process_names:
- name: redis-server
  cmdline:
  - redis-server
```
1. Have a Prometheus instance available/running, with a very low scrape interval (1s is suggested for this to get fine-grained visibility), scraping those two exporters 
1. Monitor these metrics:
  * CPU Saturation: `sum(rate(namedprocess_namegroup_thread_cpu_seconds_total{groupname="redis-server", threadname="redis-server"}[5s]))`
    * This is the data point we use for measuring the core single-threaded CPU saturation for Redis. 1.00 is full saturation 
  * Total operation rate: `sum(rate(redis_commands_total[5s]))`
  * Operation rate by command: `sum(rate(redis_commands_total[5s])) by (cmd)`
  * Time spent in operations per command: `sum(rate(redis_commands_duration_seconds_total{cmd!="flushall"}[5s])) by (cmd)`

The operations rates and time spent in operations can be compared to production when
performing initial scaling to generate production traffic levels before scaling to
test other scenarios.  

See our initial test in [November 2021](https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/1346#note_721603245)
for sizings that created plausibly production level traffic at that time.

## Capture and analyze redis trace

This repo includes a number of analyzed files (traces/\*.json) captured from production.
Remember that these traces have a limited useful shelf-life; as the application
and usages patterns change over time they will need to be refreshed.  As an estimate/guide
they are likely accurate enough for maybe 2-4 weeks, and by 3 months they should be considered
thoroughly out of date.

* Capture [Redis production
  tcpdump](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/redis/redis.md#analyze-network-traffic-on-a-redis-host).
  The output of this step is a folder of demultiplexed tcp packages generated
  by tcpflow. The following code is a simplified version:

```bash
sudo tcpdump -G 30 -W 1 -s 65535 tcp port 6379 -w redis.pcap -i $INTERFACE
rm -rf ./redis-analysis && tcpflow -I -s -o redis-analysis -r redis.pcap
```

* Run [Redis trace
  script](https://gitlab.com/gitlab-com/gl-infra/scalability/-/snippets/2192417)
  to generate an access pattern file. That snippet was copied to this repository.

```bash
ruby runbooks/scripts/redis_trace_size.rb ./redis-analysis/ > trace.json
```

* Verify the keys generated in the trace file are suitably anonymized/tokenized:

```bash
grep '^    "' trace.json|sort|uniq
```
   * In particular, look for project, user, or file names or other repeated prefixes
     where there should be a pattern match.  If necessary update [`lib/redis_trace/key_pattern.rb`](https://gitlab.com/gitlab-com/runbooks/-/blob/f21fc86f3b26a2c8de2eb969348e3d38d8c6d7b6/lib/redis_trace/key_pattern.rb)
     to handle those cases and re-run the trace creation until the results are usable.

* DO NOT COMMIT a trace until you are 100% certain it contains no project/user/filenames

## Adding commands 

We have only implemented required commands as captured by our traces to date; it is conceivable
that the application will change and start using new commands.  To do so:
1. In redis.go
   1. Add the command to [`SupportedKeys`](https://gitlab.com/gitlab-com/gl-infra/redis-load-test/-/blob/c9c3640924c4032cb9cc0143ff87d43dddd9b14a/redis.go#L36)
   1. Add a handler function
      1. To export to JS capitalize the first letter and no others, and use lower-case in JS
      1. Pass errors back as existing functions do, so that they are raised properly into JS 
   1. Add a pair of (export) functions in script.js named:
      1. <command_name> (e.g. strlen) that calls the equivalent function on client with
         arbitrary/randomly generated values as appropriate (see other existing functions
         for inspiration)
      1. prepare_<commandname> (e.g. prepare_strlen) that inserts data into Redis that
         could be returned by <command_name> if called.  Only necessary for commands which
         retrieve data from Redis, not setters.

## Limitations

* Does not handle eval: these are ignored in the original trace capture because it's hard.
  It probably does need to eventually
* Does not handle evalsha: even harder than eval, if we can't capture the original script
  from tcpdump.  Good luck!
* Does not handle `sscan`: it was low-rate and looked tricky.  It needs to be properly handled
  not just eaten
* Does not handle `memory`: also low rate, and I'm not sure it's necessary (or even where its
  coming from.
* Even when setting the rate quite low (using high TRACE_DURATION values) we still get dropped
  iterations.  This probably happens when the thread executing a high-rate command gets behind
  and cannot issue them at the rate *expected*, either because Redis is too slow or because of
  some local contention.  Normal uses of k6 wouldn't hit that (probably) when used on higher
  latency operations, but for Redis it's entirely too easy.  It's likely we'll just have to
  live with that and monitor the *results*, i.e operations/s seen on Redis, tuning and adding
  generators as necessary to get the total throughput multiples we want to test.  Adding more
  generators and raising TRACE_DURATION should help a little at least.
* K6 is resource hungry; as at Nov 2021, we need at least 3 c2-standard-8 nodes to drive production
  traffic, and multiples above to drive meaningful increases.

## Gotchas

* `Too many open files` when running the load test: `ulimit -n 10240`
