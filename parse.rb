# frozen_string_literal: true

require 'ruby_jard'
require 'json'

keys = {}
cmd = nil

def group_histogram(distribution)
  return distribution if distribution.keys.length < 15

  min ||= distribution.keys.min
  max ||= distribution.keys.max
  (min..max).each_slice((max - min) / 15).map do |slice|
    sum = 0
    distribution.each do |key, value|
      sum += value if slice.min <= key && key <= slice.max
    end
    [slice.max, sum]
  end.to_h
end

File.open('./trace.txt').each(sep="\n") do |line|
  if matches = /==== ([a-z]+) ===/i.match(line)
    cmd = matches[1]
  elsif matches = /  ([^ ]+) ({.*})$/i.match(line)
    key = matches[1]
    data = eval(matches[2])

    keys[cmd] ||= {}
    keys[cmd][key] = {
      value_size: group_histogram(data.to_a.sort_by(&:first).each_with_object({}) do |(hkey, value), memo|
        memo[hkey.to_i] = value.to_i
      end),
      response_size: {}
    }
  else
    raise "Not match: #{line.inspect}"
  end
end

File.write('./trace.json', JSON.pretty_generate(keys))
