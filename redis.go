package redis

import (
	"context"
	"time"

	"github.com/go-redis/redis/v8"
	"go.k6.io/k6/js/common"
	"go.k6.io/k6/js/modules"
)

// Register the extension on module initialization, available to
// import from JS as "k6/x/redis".
// import from JS as "k6/x/redis_cluster".
func init() {
	modules.Register("k6/x/redis", new(Redis))
	modules.Register("k6/x/redis_cluster", new(RedisCluster))
	modules.Register("k6/x/redis_sentinel", new(RedisSentinel))
}

type Redis struct{}
type RedisCluster struct{}
type RedisSentinel struct{}

// XClient represents the Client constructor (i.e. `new redis.Client()`) and
// returns a new Redis client object.
func (r *Redis) XClient(ctxPtr *context.Context, opts *redis.Options) interface{} {
	rt := common.GetRuntime(*ctxPtr)
	return common.Bind(rt, &Client{client: redis.NewClient(opts)}, ctxPtr)
}

// Similarly this exports `new redis_cluster.Client()`
func (r *RedisCluster) XClient(ctxPtr *context.Context, opts *redis.ClusterOptions) interface{} {
	rt := common.GetRuntime(*ctxPtr)
	return common.Bind(rt, &Client{client: redis.NewClusterClient(opts)}, ctxPtr)
}

func (r *RedisSentinel) XClient(ctxPtr *context.Context, opts *redis.FailoverOptions) interface{} {
	rt := common.GetRuntime(*ctxPtr)
	return common.Bind(rt, &Client{client: redis.NewFailoverClient(opts)}, ctxPtr)
}

var SupportedKeys = [...]string{
	"setex", "sadd", "get", "set", "pfadd", "expire", "hget", "exists", "incrbyfloat", "incr", "publish", "del", "zadd", "zcard", "incrby", "zremrangebyrank", "decr", "smembers", "mget", "srem", "hset", "subscribe", "unsubscribe", "zrevrange", "scard", "pexpire", "hdel", "blpop", "zrangebyscore", "zremrangebyscore", "lpush", "sismember", "hmget", "ttl", "unlink", "hmset"}

// A combined interface that represents methods on both redis.Client and redis.ClusterClient
// so we can treat it as a single type
type RedisClient interface {
	redis.Cmdable
	redis.UniversalClient
}

// Client is the Redis client wrapper.
type Client struct {
	// Might be redis.Client *or* redis.ClusterClient, but doesn't matter
	client RedisClient
}

func (c *Client) Keys() []string {
	return SupportedKeys[:]
}

// Return an empty string as the result, so that the error will be properly handled
// by the go-js bridge (see https://k6.io/blog/extending-k6-with-xk6/ "The error returned...")
func (c *Client) Set(key, value string, exp time.Duration) (string, error) {
	result := c.client.Set(c.client.Context(), key, value, time.Duration(exp)*time.Second)
	return "", result.Err()
}

func (c *Client) Setex(key, value string, exp time.Duration) (string, error) {
	result := c.client.SetEX(c.client.Context(), key, value, time.Duration(exp)*time.Second)
	return "", result.Err()
}

// In general, redis.Nil handling is not an error and we return a nil/empty response to keep JS happy.
// This is acceptable for *this* use case
func (c *Client) Get(key string) (string, error) {
	res, err := c.client.Get(c.client.Context(), key).Result()
	if err == redis.Nil {
		return "", nil
	} else if err != nil {
		return "", err
	}
	return res, nil
}

func (c *Client) Sadd(key string, member string) (int64, error) {
	res, err := c.client.SAdd(c.client.Context(), key, member).Result()
	if err != nil {
		return 0, err
	}
	return res, nil
}

func (c *Client) Scard(key string) (int64, error) {
	res, err := c.client.SCard(c.client.Context(), key).Result()
	if err != nil {
		return 0, err
	}
	return res, nil
}

func (c *Client) Srem(key string, member string) (int64, error) {
	res, err := c.client.SRem(c.client.Context(), key, member).Result()
	if err != nil {
		return 0, err
	}
	return res, nil
}

func (c *Client) Pfadd(key string, element string) (int64, error) {
	res, err := c.client.PFAdd(c.client.Context(), key, element).Result()
	if err != nil {
		return 0, err
	}
	return res, nil
}

// TTL will be in seconds;
func (c *Client) Expire(key string, ttl int) (int64, error) {
	_, err := c.client.Expire(c.client.Context(), key, time.Second*time.Duration(ttl)).Result()
	if err != nil {
		return 0, err
	}
	return 1, nil
}

func (c *Client) Pexpire(key string, ttl int) (int64, error) {
	_, err := c.client.PExpire(c.client.Context(), key, time.Millisecond*time.Duration(ttl)).Result()
	if err != nil {
		return 0, err
	}
	return 1, nil
}

func (c *Client) Hget(key string, field string) (string, error) {
	res, err := c.client.HGet(c.client.Context(), key, field).Result()
	if err == redis.Nil {
		return "", nil
	} else if err != nil {
		return "", err
	}
	return res, nil
}
func (c *Client) Hmget(key string, fields []string) ([]interface{}, error) {
	res, err := c.client.HMGet(c.client.Context(), key, fields...).Result()
	if err == redis.Nil {
		var result = make([]interface{}, 0)
		return result, nil
	} else if err != nil {
		var result = make([]interface{}, 0)
		return result, err
	}
	return res, nil
}

func (c *Client) Hset(key string, field string, value string) (int64, error) {
	res, err := c.client.HSet(c.client.Context(), key, field, value).Result()
	if err != nil {
		return 0, err
	}
	return res, nil
}

func (c *Client) Hmset(key string, field string, value string) (bool, error) {
	res, err := c.client.HMSet(c.client.Context(), key, field, value).Result()
	if err != nil {
		return false, err
	}
	return res, nil
}

func (c *Client) Hdel(key string, field string) (int64, error) {
	res, err := c.client.HDel(c.client.Context(), key, field).Result()
	if err != nil {
		return 0, err
	}
	return res, nil
}

func (c *Client) Exists(key string) (int64, error) {
	res, err := c.client.Exists(c.client.Context(), key).Result()
	if err != nil {
		return 0, err
	}
	return res, nil
}

func (c *Client) Incrbyfloat(key string, value float64) (float64, error) {
	res, err := c.client.IncrByFloat(c.client.Context(), key, value).Result()
	if err != nil {
		return 0.0, err
	}
	return res, nil
}

func (c *Client) Incr(key string) (int64, error) {
	res, _ := c.client.Incr(c.client.Context(), key).Result()
	return res, nil
}

func (c *Client) Publish(channel string, message string) (int64, error) {
	res, err := c.client.Publish(c.client.Context(), channel, message).Result()
	if err != nil {
		return 0, err
	}
	return res, nil
}

func (c *Client) Subscribe(channel string, durationMs int, wait bool) error {
	pubsub := c.client.Subscribe(context.Background(), channel)
	go func(pubsub *redis.PubSub) {
		for {
			if _, err := pubsub.ReceiveMessage(context.Background()); err != nil {
				// Maybe because the pubsub is closed
				break
			}
		}
	}(pubsub)
	if wait {
		time.Sleep(time.Millisecond * time.Duration(durationMs))
		pubsub.Close()
	} else {
		time.AfterFunc(time.Millisecond*time.Duration(durationMs), func() {
			pubsub.Close()
		})
	}
	return nil
}

func (c *Client) Del(key string) (int64, error) {
	res, err := c.client.Del(c.client.Context(), key).Result()
	if err != nil {
		return 0, err
	}
	return res, nil
}

func (c *Client) Zadd(key string, score float64, member string) (int64, error) {
	res, err := c.client.ZAdd(c.client.Context(), key, &redis.Z{
		Score:  score,
		Member: member,
	}).Result()
	if err != nil {
		return 0, err
	}
	return res, nil
}

func (c *Client) Zcard(key string) (int64, error) {
	res, err := c.client.ZCard(c.client.Context(), key).Result()
	if err != nil {
		return 0, err
	}
	return res, nil
}

func (c *Client) Incrby(key string, value int64) (int64, error) {
	res, err := c.client.IncrBy(c.client.Context(), key, value).Result()
	if err != nil {
		return 0, err
	}
	return res, nil
}

func (c *Client) Zremrangebyrank(key string, start int64, stop int64) (int64, error) {
	res, err := c.client.ZRemRangeByRank(c.client.Context(), key, start, stop).Result()
	if err != nil {
		return 0, err
	}
	return res, nil
}

func (c *Client) Decr(key string) (int64, error) {
	res, err := c.client.Decr(c.client.Context(), key).Result()
	if err != nil {
		return 0, err
	}
	return res, nil
}

func (c *Client) Smembers(key string) ([]string, error) {
	res, err := c.client.SMembers(c.client.Context(), key).Result()
	if err == redis.Nil {
		return []string{}, nil
	} else if err != nil {
		return []string{}, err
	}
	return res, nil
}

func (c *Client) Mget(keys []string) ([]interface{}, error) {
	res, err := c.client.MGet(c.client.Context(), keys...).Result()
	if err == redis.Nil {
		var result = make([]interface{}, 0)
		return result, nil
	} else if err != nil {
		var result = make([]interface{}, 0)
		return result, err
	}
	return res, nil
}

func (c *Client) Zrevrange(key string, start, stop int64) ([]string, error) {
	res, err := c.client.ZRevRange(c.client.Context(), key, start, stop).Result()
	if err == redis.Nil {
		return []string{}, nil
	} else if err != nil {
		return []string{}, err
	}
	return res, nil
}

func (c *Client) Blpop(duration int64, key string) ([]string, error) {
	res, err := c.client.BLPop(c.client.Context(), time.Second*time.Duration(duration), key).Result()
	if err == redis.Nil {
		return []string{}, nil
	} else if err != nil {
		return []string{}, err
	}
	return res, nil
}

func (c *Client) Zrangebyscore(key, min, max string) ([]string, error) {
	res, err := c.client.ZRangeByScore(c.client.Context(), key, &redis.ZRangeBy{
		Min: min,
		Max: max,
	}).Result()
	if err == redis.Nil {
		return []string{}, nil
	} else if err != nil {
		return []string{}, err
	}
	return res, nil
}

func (c *Client) Zremrangebyscore(key, min, max string) (int64, error) {
	res, err := c.client.ZRemRangeByScore(c.client.Context(), key, min, max).Result()
	if err != nil {
		return 0, err
	}
	return res, nil
}

func (c *Client) Lpush(key, element string) (int64, error) {
	res, err := c.client.LPush(c.client.Context(), key, element).Result()
	if err != nil {
		return 0, err
	}
	return res, nil
}

func (c *Client) Sismember(key, member string) (bool, error) {
	res, err := c.client.SIsMember(c.client.Context(), key, member).Result()
	if err != nil {
		return false, err
	}
	return res, nil
}

func (c *Client) Ttl(key string) (int64, error) {
	res, err := c.client.TTL(c.client.Context(), key).Result()
	if err != nil {
		return -1, err
	}
	return int64(res / time.Second), nil
}

func (c *Client) Unlink(key string) (int64, error) {
	res, err := c.client.Unlink(c.client.Context(), key).Result()
	if err != nil {
		return 0, err
	}
	return res, nil
}
